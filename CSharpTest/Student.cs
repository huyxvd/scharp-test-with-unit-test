﻿using System;

namespace CSharpTest
{
    public class Student
    {
        public int Id { get; set; }

        public int Grade { get; set; }

        public string Name { get; set; }

        public DateTime DateOfBirth { get; set; }

        public double MathScore { get; set; }

        public double PhysicScore { get; set; }

        public double ChemicalScore { get; set; }

        public string Gender { get; set; }

        public string Phone { get; set; }
    }
}

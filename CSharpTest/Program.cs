﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CSharpTest
{
    public class Program
    {
        static void Main(string[] args)
        {
            StreamReader r = new StreamReader("students.json");

            string json = r.ReadToEnd();
            List<Student> students = JsonConvert.DeserializeObject<List<Student>>(json);

            Console.WriteLine("Hello World!");
        }

        // tiêu chí dựa trên điểm trung bình của 3 môn toán lý hóa
        // chưa đạt  < 6
        // 6 <= đạt < 8,5
        //  xuất xắc >= 8,5

        // lấy danh sách học sinh xuất xắc khối 12
        // xếp thành tích giảm dần
        public static List<Student> ExellenStudentWithGrade12(List<Student> students)
        {
            // write code here
            return null;
        }

        // lấy danh sách những học sinh có thành tích đạt
        // sắp xếp theo thành tích tăng
        public static List<Student> AvgStudent(List<Student> students)
        {
            // write code here
            return null;
        }

        // lấy danh sách học sinh lớp 10 và sắp xếp theo tên từ a => z
        public static List<Student> StudentGrate10(List<Student> students)
        {
            // write code here
            return null;
        }

        // top 3 học sinh có điểm trung bình cao nhất mỗi khối theo thứ tự điểm giảm dần
        // 3 bạn từ lớp 10, 3 bạn từ lớp 11 và 3 bạn từ lớp 12 mỗi khối
        public static List<Student> Top3EveryGrade(List<Student> students)
        {
            // write code here
            return null;
        }

        // thống kê số học sinh theo từng tháng 
        // kết quả trả về kiểu dictionary số tháng là key và số học sinh sẽ là value
        public static Dictionary<int, int> MonthOfBirthWithStudentCount(List<Student> students)
        {
            // write code here
            return null;
        }

        // tính thành tích trung bình của từng khối lần lượt là 10, 11, và 12
        public static double[] AvgScoreOfEveryGrade(List<Student> students)
        {
            // write code here
            return null;
        }

        // tính tỉ lệ học sinh lên lớp của từng khối lần lượt là 10, 11, và 12
        public static double[] PercentateOfStudentCanGoToTheNextGrade(List<Student> students)
        {
            // write code here
            return null;
        }

        // top 10 => 20 của những học sinh có thành tích cao nhất
        // xếp theo tên
        public static string[] Top10to20StudentHighestAVGScore(List<Student> students)
        {
            // write code here
            return null;
        }

        // lấy danh sách học sinh có điểm cao hơn điểm trung bình của toàn khối
        // sắp xếp theo tên
        public static List<Student> StudentWhoseHadBetterScoreThanAVG(List<Student> students)
        {
            // write code here
            return null;
        }

        // tìm ra tháng(dựa trên ngày sinh) mà ở đó thành tích của học sinh là tốt nhất
        public static int MonthHaveBestAVGScore(List<Student> students)
        {
            // write code here
            return -1;
        }

        // danh sách học sinh sinh vào tháng 3 năm 2000
        // xếp theo tên
        public static List<Student> StudentBirthInFeb2020(List<Student> students)
        {
            // write code here
            return null;
        }


        // danh sách học sinh có sđt đuôi là 50
        // xếp theo tên
        public static List<Student> StudentWithPhoneNum(List<Student> students)
        {
            // write code here
            return null;
        }

        // top 3 bạn nữ có thành tích xuất xắc khối 12
        // xếp theo thành tích sau đó theo tên
        public static List<Student> FemaleBestAVG(List<Student> students)
        {
            // write code here
            return null;
        }

        // danh sách học sinh nữ sinh vào lúc 6h sáng
        // xếp theo Id
        public static List<Student> FemaleBirtOn6AM(List<Student> students)
        {
            // write code here
            return null;
        }

        // điểm trung bình của học sinh đầu tiên
        public static double AVGScoreFirstStudent(List<Student> students)
        {
            // write code here
            return -1;
        }

        // điểm trung bình của học sinh cuối cung
        public static double AVGScoreLastStudent(List<Student> students)
        {
            // write code here
            return -1;
        }

        // điểm trung bình của học sinh đứng vị trí thứ 100
        public static double AVGScoreStudent100th(List<Student> students)
        {
            // write code here
            return -1;
        }
    }
}

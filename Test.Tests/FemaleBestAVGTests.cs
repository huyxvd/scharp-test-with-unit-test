using AutoFixture;
using CSharpTest;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Test.Tests
{
    public class FemaleBestAVGTests : SetUpTest
    {
        [Fact]
        public void FemaleBestAVG_WithStudent1Data()
        {
            var mockData = new List<Student>
            {
                new Student
                {
                    Id = 1,
                    Grade = 10,
                    Gender = "female",
                    MathScore = 9.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 7.23
                },
                new Student
                {
                    Id = 2,
                    Grade = 11,
                    Gender = "female",
                    MathScore = 7.23,
                    PhysicScore = 6.23,
                    ChemicalScore = 5.23
                },
                new Student
                {
                    Id = 3,
                    Name = "B",
                    Gender = "female",
                    Grade = 12,
                    MathScore = 8.23,
                    PhysicScore = 9.23,
                    ChemicalScore = 9.23
                },
                new Student
                {
                    Id = 4,
                    Name = "A",
                    Gender = "female",
                    Grade = 12,
                    MathScore = 10,
                    PhysicScore = 10,
                    ChemicalScore = 10
                },
            };


            var expected = new List<Student>
            {
                new Student
                {
                    Id = 4,
                    Name = "A",
                    Gender = "female",
                    Grade = 12,
                    MathScore = 10,
                    PhysicScore = 10,
                    ChemicalScore = 10
                },
                new Student
                {
                    Id = 3,
                    Name = "B",
                    Gender = "female",
                    Grade = 12,
                    MathScore = 8.23,
                    PhysicScore = 9.23,
                    ChemicalScore = 9.23
                },
            };

            var result = Program.FemaleBestAVG(mockData);


            result.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void FemaleBestAVG_With3Student()
        {
            var mockData = new List<Student>
            {
                new Student
                {
                    Id = 0,
                    Name = "F",
                    Gender = "male",
                    Grade = 12,
                    MathScore = 10,
                    PhysicScore = 10,
                    ChemicalScore = 10
                },
                new Student
                {
                    Id = 1,
                    Grade = 10,
                    Gender = "female",
                    MathScore = 9.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 7.23
                },
                new Student
                {
                    Id = 2,
                    Grade = 11,
                    Gender = "female",
                    MathScore = 7.23,
                    PhysicScore = 6.23,
                    ChemicalScore = 5.23
                },
                new Student
                {
                    Id = 3,
                    Name = "B",
                    Gender = "female",
                    Grade = 12,
                    MathScore = 8.23,
                    PhysicScore = 9.23,
                    ChemicalScore = 9.23
                },
                new Student
                {
                    Id = 4,
                    Name = "A",
                    Gender = "female",
                    Grade = 12,
                    MathScore = 10,
                    PhysicScore = 10,
                    ChemicalScore = 10
                },
            };


            var expected = new List<Student>
            {
                new Student
                {
                    Id = 4,
                    Name = "A",
                    Gender = "female",
                    Grade = 12,
                    MathScore = 10,
                    PhysicScore = 10,
                    ChemicalScore = 10
                },
                new Student
                {
                    Id = 3,
                    Name = "B",
                    Gender = "female",
                    Grade = 12,
                    MathScore = 8.23,
                    PhysicScore = 9.23,
                    ChemicalScore = 9.23
                }
            };

            var result = Program.FemaleBestAVG(mockData);


            result.Should().BeEquivalentTo(expected);
        }
    }
}

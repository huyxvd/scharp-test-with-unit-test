using AutoFixture;
using CSharpTest;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Test.Tests
{
    public class AvgStudentTests : SetUpTest
    {
        [Fact]
        public void AvgStudent_WithStudent1Data()
        {
            var mockData = new List<Student>
            {
                new Student
                {
                    Id = 1,
                    Grade = 10,
                    MathScore = 9.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 7.23
                },
                new Student
                {
                    Id = 2,
                    Grade = 11,
                    MathScore = 7.23,
                    PhysicScore = 6.23,
                    ChemicalScore = 5.23
                },
                new Student
                {
                    Id = 3,
                    Grade = 12,
                    MathScore = 1.23,
                    PhysicScore = 2.23,
                    ChemicalScore = 3.23
                },
                new Student
                {
                    Id = 4,
                    Grade = 12,
                    MathScore = 9.23,
                    PhysicScore = 9.23,
                    ChemicalScore = 9.23
                },
            };

            var expected = new List<Student>
            {
                new Student
                {
                    Id = 2,
                    Grade = 11,
                    MathScore = 7.23,
                    PhysicScore = 6.23,
                    ChemicalScore = 5.23
                },
                new Student
                {
                    Id = 1,
                    Grade = 10,
                    MathScore = 9.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 7.23
                },
            };

            var result = Program.AvgStudent(mockData);


            result.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void AvgStudent_WithStudent1Data2()
        {
            var mockData = new List<Student>
            {
                new Student
                {
                    Id = 1,
                    Grade = 10,
                    MathScore = 9.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 7.23
                },
                new Student
                {
                    Id = 2,
                    Grade = 11,
                    MathScore = 7.23,
                    PhysicScore = 6.23,
                    ChemicalScore = 5.23
                },
                new Student
                {
                    Id = 3,
                    Grade = 12,
                    MathScore = 1.23,
                    PhysicScore = 2.23,
                    ChemicalScore = 3.23
                },
                new Student
                {
                    Id = 4,
                    Grade = 11,
                    MathScore = 10,
                    PhysicScore = 10,
                    ChemicalScore = 10
                },
            };

            var expected = new List<Student>
            {
                new Student
                {
                    Id = 2,
                    Grade = 11,
                    MathScore = 7.23,
                    PhysicScore = 6.23,
                    ChemicalScore = 5.23
                },
                new Student
                {
                    Id = 1,
                    Grade = 10,
                    MathScore = 9.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 7.23
                },
            };

            var result = Program.AvgStudent(mockData);


            result.Should().BeEquivalentTo(expected);
        }
    }
}

using AutoFixture;
using CSharpTest;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Test.Tests
{
    public class StudentWithPhoneNumTests : SetUpTest
    {
        [Fact]
        public void StudentWithPhoneNum_WithStudent1Data()
        {
            var mocks = new List<Student>
            {
                new Student
                {
                    Name = "F",
                    Phone = "123450"
                },
                new Student
                {
                    Name = "B",
                    Phone = "123450 "
                },
                new Student
                {
                    Name = "A",
                    Phone = "50"
                },
            };

            var expected = new List<Student>
            {
                new Student
                {
                    Name = "A",
                    Phone = "50"
                },
                new Student
                {
                    Name = "F",
                    Phone = "123450"
                },
            };

            var result = Program.StudentWithPhoneNum(mocks);


            result.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void StudentWithPhoneNum_ShouldReturnEmptyData()
        {
            var mocks = _fixture.Build<Student>()
                .With(x => x.Phone, "0987750")
                .CreateMany(10)
                .ToList();
            
            var expected = mocks.OrderBy(x => x.Name).ToList();

            var result = Program.StudentWithPhoneNum(mocks);

            result.Should().BeEquivalentTo(expected);
        }
    }
}

﻿using CSharpTest;
using FluentAssertions;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Test.Tests
{
    public class StudentWhoseHadBetterScoreThanAVGTests : SetUpTest
    {
        [Fact]
        public void StudentWhoseHadBetterScoreThanAVG_WithStudent1Data()
        {
            var mockData = new List<Student>
            {
                new Student
                {
                    Id = 1,
                    Name = "A",
                    Grade = 10,
                    MathScore = 9.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {

                    Id = 4,
                    Grade = 10,
                    MathScore = 1.23,
                    PhysicScore = 1.23,
                    ChemicalScore = 1.23
                },
                new Student
                {
                    Id = 9,
                    Name = "C",
                    Grade = 12,
                    MathScore = 9.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {

                    Id = 5,
                    Name = "B",
                    Grade = 11,
                    MathScore = 9.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {
                    Id = 8,
                    Grade = 11,
                    MathScore = 1.23,
                    PhysicScore = 1.23,
                    ChemicalScore = 1.23
                },
                new Student
                {
                    Id = 12,
                    Grade = 12,
                    MathScore = 1.23,
                    PhysicScore = 1.23,
                    ChemicalScore = 1.23
                }
            };

            var expected = new List<Student>
            {
                new Student
                {
                    Id = 1,
                    Grade = 10,
                    Name = "A",
                    MathScore = 9.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {

                    Id = 5,
                    Name = "B",
                    Grade = 11,
                    MathScore = 9.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {
                    Id = 9,
                    Name = "C",
                    Grade = 12,
                    MathScore = 9.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                }
            };

            var result = Program.StudentWhoseHadBetterScoreThanAVG(mockData);

            result.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void StudentWhoseHadBetterScoreThanAVG_ShouldReturnCorrectData()
        {
            var mockData = new List<Student>
            {
                new Student
                {
                    Id = 1,
                    Name = "Z",
                    Grade = 10,
                    MathScore = 9.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {
                    Id = 2,
                    Name = "C",
                    Grade = 10,
                    MathScore = 9.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {
                    Id = 4,
                    Grade = 10,
                    MathScore = 1.23,
                    PhysicScore = 1.23,
                    ChemicalScore = 1.23
                },
                new Student
                {
                    Id = 5,
                    Name = "ZZ",
                    Grade = 11,
                    MathScore = 1.23,
                    PhysicScore = 1.23,
                    ChemicalScore = 1.23
                },
                new Student
                {
                    Id = 5,
                    Name = "ZZ",
                    Grade = 12,
                    MathScore = 1.23,
                    PhysicScore = 1.23,
                    ChemicalScore = 1.23
                },
                new Student
                {
                    Id = 12,
                    Name = "A",
                    Grade = 12,
                    MathScore = 5.23,
                    PhysicScore = 5.23,
                    ChemicalScore = 5.23
                }

            };

            var expected = new List<Student>
            {
                new Student
                {
                    Id = 12,
                    Name = "A",
                    Grade = 12,
                    MathScore = 5.23,
                    PhysicScore = 5.23,
                    ChemicalScore = 5.23
                },
                new Student
                {
                    Id = 2,
                    Name = "C",
                    Grade = 10,
                    MathScore = 9.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {
                    Id = 1,
                    Name = "Z",
                    Grade = 10,
                    MathScore = 9.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                }
            };

            var result = Program.StudentWhoseHadBetterScoreThanAVG(mockData);

            result.Should().BeEquivalentTo(expected);
        }
    }
}

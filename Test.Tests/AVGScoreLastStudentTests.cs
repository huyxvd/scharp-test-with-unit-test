using AutoFixture;
using CSharpTest;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Test.Tests
{
    public class AVGScoreLastStudentTests : SetUpTest
    {
        [Fact]
        public void AVGScoreLastStudent_WithStudent1Data()
        {
            var mockData = new List<Student>
            {
                new Student
                {
                    Id = 1,
                    Grade = 10,
                    MathScore = 9.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 7.23
                },
                new Student
                {
                    Id = 2,
                    Grade = 10,
                    MathScore = 10.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
            };
            var expected = 9.23;

            var result = Program.AVGScoreLastStudent(mockData);


            result.Should().Be(expected);
        }

        [Fact]
        public void AVGScoreLastStudent_WithMockData()
        {
            var mockData = new List<Student>
            {
                new Student
                {
                    Id = 1,
                    Grade = 10,
                    MathScore = 9.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 7.23
                },
                new Student
                {
                    Id = 2,
                    Grade = 10,
                    MathScore = 1.23,
                    PhysicScore = 1.23,
                    ChemicalScore = 1.23
                },
                new Student
                {
                    Id = 2,
                    Grade = 10,
                    MathScore = 7.23,
                    PhysicScore = 6.23,
                    ChemicalScore = 5.23
                },
            };
            var expected = 6.23;

            var result = Program.AVGScoreLastStudent(mockData);


            result.Should().Be(expected);
        }
    }
}

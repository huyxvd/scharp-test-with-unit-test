using CSharpTest;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Test.Tests
{
    public class MonthOfBirthWithStudentCountTests : SetUpTest
    {
        [Fact]
        public void MonthOfBirthWithStudentCount_WithStudent1Data()
        {
            var mockData = new List<Student>
            {
                new Student
                {
                    DateOfBirth = new System.DateTime(2011, 1, 2),
                },
                new Student
                {
                    DateOfBirth = new System.DateTime(2011, 1, 2),
                },
                new Student
                {
                    DateOfBirth = new System.DateTime(2011, 3, 2),
                },
                new Student
                {
                    DateOfBirth = new System.DateTime(2011, 4, 2),
                }
            };

            var expected = new Dictionary<int, int>
            {
                { 1, 2 },
                { 3, 1 },
                { 4, 1 },
                { 2, 0 },
                { 5, 0 },
                { 6, 0 },
                { 7, 0 },
                { 8, 0 },
                { 9, 0 },
                { 10, 0 },
                { 11, 0 },
                { 12, 0 },
            };
            var result = Program.MonthOfBirthWithStudentCount(mockData);


            result.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void MonthOfBirthWithStudentCount_ShouldReturnCorrectData()
        {
            var mockData = new List<Student>
            {
                new Student
                {
                    DateOfBirth = new System.DateTime(2011, 1, 2),
                },
                new Student
                {
                    DateOfBirth = new System.DateTime(2011, 1, 2),
                },
                new Student
                {
                    DateOfBirth = new System.DateTime(2011, 1, 2),
                },
                new Student
                {
                    DateOfBirth = new System.DateTime(2011, 11, 2),
                },
                new Student
                {
                    DateOfBirth = new System.DateTime(2011, 11, 2),
                },
                new Student
                {
                    DateOfBirth = new System.DateTime(2011, 11, 2),
                },
                new Student
                {
                    DateOfBirth = new System.DateTime(2011, 2, 2),
                },
                new Student
                {
                    DateOfBirth = new System.DateTime(2011, 2, 2),
                },
                new Student
                {
                    DateOfBirth = new System.DateTime(2011, 2, 2),
                },
            };

            var expected = new Dictionary<int, int>
            {
                { 2, 3 },
                { 11, 3 },
                { 1, 3 },
                { 3, 0 },
                { 4, 0 },
                { 5, 0 },
                { 6, 0 },
                { 7, 0 },
                { 8, 0 },
                { 9, 0 },
                { 10, 0 },
                { 12, 0 },
            };
            var result = Program.MonthOfBirthWithStudentCount(mockData);


            result.Should().BeEquivalentTo(expected);
        }
    }
}

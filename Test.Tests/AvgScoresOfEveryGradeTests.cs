﻿using AutoFixture;
using CSharpTest;
using FluentAssertions;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Test.Tests
{
    public class AvgScoresOfEveryGradeTests : SetUpTest
    {
        [Fact]
        public void AvgScoresOfEveryGrade_WithStudent1Data()
        {
            var mockg10 = _fixture.Build<Student>()
                .With(x => x.Grade, 10)
                .With(x => x.MathScore, 9.5)
                .With(x => x.ChemicalScore, 7.5)
                .With(x => x.PhysicScore, 8.5)
                .CreateMany(10).ToList();

            var mockg11 = _fixture.Build<Student>()
                .With(x => x.Grade, 11)
                .With(x => x.MathScore, 6.5)
                .With(x => x.ChemicalScore, 7.5)
                .With(x => x.PhysicScore, 8.5)
                .CreateMany(10).ToList();

            var mockg12 = _fixture.Build<Student>()
                .With(x => x.Grade, 12)
                .With(x => x.MathScore, 6.5)
                .With(x => x.ChemicalScore, 7.5)
                .With(x => x.PhysicScore, 5.5)
                .CreateMany(10).ToList();

            var items = mockg10.Concat(mockg11).Concat(mockg12).ToList();

            var expected = new double[] { 8.5, 7.5, 6.5 };

            var result = Program.AvgScoreOfEveryGrade(items);

            result.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void AvgScoresOfEveryGrade_WithMockData()
        {
            var mockData = new List<Student>
            {
                new Student
                {
                    Id = 1,
                    Grade = 10,
                    MathScore = 9.5,
                    PhysicScore = 8.5,
                    ChemicalScore = 7.5
                },
                new Student
                {
                    Id = 1,
                    Grade = 10,
                    MathScore = 9.5,
                    PhysicScore = 8.5,
                    ChemicalScore = 7.5
                },
                new Student
                {
                    Id = 2,
                    Grade = 11,
                    MathScore = 7.5,
                    PhysicScore = 6.5,
                    ChemicalScore = 5.5
                },
                new Student
                {
                    Id = 3,
                    Grade = 12,
                    MathScore = 1.5,
                    PhysicScore = 2.5,
                    ChemicalScore = 3.5
                },
            };
            var expected = new double[] { 8.5, 6.5, 2.5 };

            var result = Program.AvgScoreOfEveryGrade(mockData);

            result.Should().BeEquivalentTo(expected);
        }
    }
}

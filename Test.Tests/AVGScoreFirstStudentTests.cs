using AutoFixture;
using CSharpTest;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Test.Tests
{
    public class AVGScoreFirstStudentTests : SetUpTest
    {
        [Fact]
        public void AVGScoreFirstStudent_WithStudent1Data()
        {
            var mockData = new List<Student>
            {
                new Student
                {
                    Id = 2,
                    Grade = 10,
                    MathScore = 1.13,
                    PhysicScore = 2.23,
                    ChemicalScore = 3.33
                },
            };

            var result = Program.AVGScoreFirstStudent(mockData);


            result.Should().Be(2.23);
        }


        [Fact]
        public void AVGScoreFirstStudent_WithMockData()
        {
            var mockData = new List<Student>
            {
                new Student
                {
                    Id = 1,
                    Grade = 10,
                    MathScore = 9.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 7.23
                },
                new Student
                {
                    Id = 2,
                    Grade = 10,
                    MathScore = 8.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
            };
            var expected = 8.23;

            var result = Program.AVGScoreFirstStudent(mockData);


            result.Should().Be(expected);
        }
    }
}

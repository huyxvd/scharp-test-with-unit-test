﻿using AutoFixture;
using CSharpTest;
using FluentAssertions;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Test.Tests
{
    public class Top10To20StudentHighestAVGScoreTests : SetUpTest
    {
        [Fact]
        public void Top10To20StudentHighestAVGScore_WithStudent1Data()
        {
            var mocks = _fixture.Build<Student>()
                .With(x => x.MathScore, 10)
                .With(x => x.PhysicScore, 10)
                .With(x => x.ChemicalScore, 10)
                .CreateMany(9)
                .ToList();

            var mockp1 = new List<Student>
            {
                new Student
                {
                    Name = "14",
                    PhysicScore = 0.0,
                },
                new Student
                {
                    Name = "1",
                    PhysicScore = 9.5,
                },
                new Student
                {
                    Name = "2",
                    PhysicScore = 9.5,
                },
                new Student
                {
                    Name = "3",
                    PhysicScore = 9.5,
                },
                new Student
                {
                    Name = "4",
                    PhysicScore = 9.5,
                },
                new Student
                {
                    Name = "5",
                    PhysicScore = 9.5,
                },
                new Student
                {
                    Name = "6",
                    PhysicScore = 9.5,
                },
                new Student
                {
                    Name = "7",
                    PhysicScore = 9.5,
                },
                new Student
                {
                    Name = "8",
                    PhysicScore = 9.5,
                },
                new Student
                {
                    Name = "9",
                    PhysicScore = 7.5,
                },
                new Student
                {
                    Name = "10",
                    PhysicScore = 8.5,
                },
                new Student
                {
                    Name = "11",
                    PhysicScore = 9.5,
                },
                new Student
                {
                    Name = "12",
                    PhysicScore = 1,
                },
                new Student
                {
                    Name = "13",
                    PhysicScore = 0.5,
                },
            };
            var mockData = mocks.Concat(mockp1).ToList();

            var expected = new string[] { "1", "2", "3", "4", "5", "6", "7", "8", "11", "10", "9" };

            var result = Program.Top10to20StudentHighestAVGScore(mockData);

            result.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void Top10To20StudentHighestAVGScore_ShouldReturnEmptyData()
        {
            var mocks = _fixture.Build<Student>()
                .With(x => x.MathScore, 10)
                .With(x => x.PhysicScore, 10)
                .With(x => x.ChemicalScore, 10)
                .CreateMany(9)
                .ToList();

            var mockp1 = new List<Student>
            {
                new Student
                {
                    Name = "1",
                    PhysicScore = 9.5,
                },
                new Student
                {
                    Name = "1",
                    PhysicScore = 9.5,
                },
                new Student
                {
                    Name = "2",
                    PhysicScore = 9.5,
                },
                new Student
                {
                    Name = "2",
                    PhysicScore = 9.5,
                },
                new Student
                {
                    Name = "4",
                    PhysicScore = 9.5,
                },
                new Student
                {
                    Name = "4",
                    PhysicScore = 9.5,
                },
                new Student
                {
                    Name = "4",
                    PhysicScore = 9.5,
                }
            };
            var mockData = mocks.Concat(mockp1).ToList();

            var expected = new string[] { "1", "1", "2", "2", "4", "4", "4"};

            var result = Program.Top10to20StudentHighestAVGScore(mockData);

            result.Should().BeEquivalentTo(expected);
        }
    }
}

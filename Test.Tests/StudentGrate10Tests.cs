using AutoFixture;
using CSharpTest;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Test.Tests
{
    public class StudentGrate10Tests : SetUpTest
    {
        [Fact]
        public void StudentGrate10_WithStudent1Data()
        {
            var mockData = new List<Student>
            {
                new Student
                {
                    Id = 3,
                    Grade = 11,
                    Name = "A"
                },
                new Student
                {
                    Id = 1,
                    Grade = 10,
                    Name = "F"
                },
                new Student
                {
                    Id = 2,
                    Grade = 10,
                    Name = "D"
                },
                new Student
                {
                    Id = 3,
                    Grade = 10,
                    Name = "A"
                },
                new Student
                {
                    Id = 3,
                    Grade = 11,
                    Name = "A"
                }
            };

            var expected = new List<Student>
            {
                new Student
                {
                    Id = 1,
                    Grade = 10,
                    Name = "F"
                },
                new Student
                {
                    Id = 2,
                    Grade = 10,
                    Name = "D"
                },
                new Student
                {
                    Id = 3,
                    Grade = 10,
                    Name = "A"
                },
            };

            var result = Program.StudentGrate10(mockData);


            result.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void StudentGrate10_ShouldReturnEmptyData()
        {
            var student11 = _fixture.Build<Student>()
                .With(x => x.Grade, 11)
                .CreateMany()
                .ToList();

            var expected = new List<Student>
            {
                new Student
                {
                    Id = 1,
                    Grade = 10,
                    Name = "F"
                },
                new Student
                {
                    Id = 2,
                    Grade = 10,
                    Name = "D"
                },
                new Student
                {
                    Id = 3,
                    Grade = 10,
                    Name = "A"
                },
            };
            var mocks = student11.Concat(expected).ToList();

            var result = Program.StudentGrate10(mocks);


            result.Should().BeEquivalentTo(expected);
        }
    }
}

﻿using CSharpTest;
using System;
using System.Collections.Generic;
using System.Text;

namespace Test.Tests
{
    public static class StudentExtension
    {
        public static double StudentAvg(this Student student)
        {
            return (student.MathScore + student.ChemicalScore + student.PhysicScore) / 3;
        }
    }
}

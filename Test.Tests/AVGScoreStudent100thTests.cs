using AutoFixture;
using CSharpTest;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Test.Tests
{
    public class AVGScoreStudent100thTests : SetUpTest
    {
        [Fact]
        public void AVGScoreStudent100th_WithStudent1Data()
        {
            var mockData = _fixture.Build<Student>().With(x => x.Grade, 10)
                .CreateMany(99)
                .ToList();

            var student100th = new Student
            {
                Id = 10000,
                Grade = 10,
                MathScore = 9.03,
                PhysicScore = 8.13,
                ChemicalScore = 7.23
            };

            mockData.Add(student100th);

            var exptected = 8.13;

            var result = Program.AVGScoreStudent100th(mockData);


            result.Should().Be(exptected);
        }

        [Fact]
        public void AvgScoresOfEveryGrade_WithMockData()
        {
            var mockData = _fixture.Build<Student>().With(x => x.Grade, 10)
                .CreateMany(99)
                .ToList();

            var student100th = new Student
            {
                Id = 10000,
                Grade = 10,
                MathScore = 9.23,
                PhysicScore = 8.33,
                ChemicalScore = 7.43
            };

            mockData.Add(student100th);

            var exptected = 8.33;

            var result = Program.AVGScoreStudent100th(mockData);


            result.Should().Be(exptected);
        }
    }
}

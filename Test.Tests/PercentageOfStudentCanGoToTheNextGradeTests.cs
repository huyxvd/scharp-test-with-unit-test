﻿using CSharpTest;
using FluentAssertions;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Test.Tests
{
    public class PercentageOfStudentCanGoToTheNextGradeTests : SetUpTest
    {
        [Fact]
        public void PercentageOfStudentCanGoToTheNextGrade_WithStudent1Data()
        {
            var mockData = new List<Student>
            {
                new Student
                {
                    Id = 1,
                    Grade = 10,
                    MathScore = 9.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {
                    Id = 2,
                    Grade = 10,
                    MathScore = 8.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {
                    Id = 3,
                    Grade = 10,
                    MathScore = 7.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {

                    Id = 4,
                    Grade = 10,
                    MathScore = 1.23,
                    PhysicScore = 1.23,
                    ChemicalScore = 1.23
                },
                new Student
                {

                    Id = 5,
                    Grade = 11,
                    MathScore = 9.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {
                    Id = 8,
                    Grade = 11,
                    MathScore = 1.23,
                    PhysicScore = 1.23,
                    ChemicalScore = 1.23
                },
                new Student
                {
                    Id = 9,
                    Grade = 12,
                    MathScore = 9.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {

                    Id = 10,
                    Grade = 12,
                    MathScore = 1.23,
                    PhysicScore = 1.23,
                    ChemicalScore = 1.23
                },
                new Student
                {
                    Id = 12,
                    Grade = 12,
                    MathScore = 1.23,
                    PhysicScore = 1.23,
                    ChemicalScore = 1.23
                },
                new Student
                {
                    Id = 13,
                    Grade = 12,
                    MathScore = 1.23,
                    PhysicScore = 1.23,
                    ChemicalScore = 1.23
                }
            };

            // arrange
            var expectedResult = new double[]
            { 
                75,
                50,
                25
            };

            // action
            var result = Program.PercentateOfStudentCanGoToTheNextGrade(mockData);

            // assert
            double precision = 0.01;
            result.Should().BeEquivalentTo(expectedResult, options => options
                .Using<double>(ctx => ctx.Subject.Should().BeApproximately(ctx.Expectation, precision))
                .WhenTypeIs<double>());
        }

        [Fact]
        public void PercentageOfStudentCanGoToTheNextGrade_ShouldReturnCorrectData()
        {
            var mockData = new List<Student>
            {
                new Student
                {
                    Id = 1,
                    Grade = 10,
                    MathScore = 9.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {

                    Id = 5,
                    Grade = 11,
                    MathScore = 9.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {

                    Id = 10,
                    Grade = 12,
                    MathScore = 1.23,
                    PhysicScore = 1.23,
                    ChemicalScore = 1.23
                }
            };

            // arrange
            var expectedResult = new double[]
            {
                100,
                100,
                0
            };

            // action
            var result = Program.PercentateOfStudentCanGoToTheNextGrade(mockData);

            // assert
            double precision = 0.01;
            result.Should().BeEquivalentTo(expectedResult, options => options
                .Using<double>(ctx => ctx.Subject.Should().BeApproximately(ctx.Expectation, precision))
                .WhenTypeIs<double>());
        }
    }
}

﻿using CSharpTest;
using FluentAssertions;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Test.Tests
{
    public class MonthHasBestAVGScoreTests : SetUpTest
    {
        [Fact]
        public void MonthHasBestAVGScore_ShouldReturnCorrectData()
        {
            var mockData = new List<Student>
            {
                new Student
                {
                    Id = 11,
                    DateOfBirth = new System.DateTime(2011, 1, 2),
                    Grade = 10,
                    MathScore = 1,
                    PhysicScore = 1,
                    ChemicalScore = 10
                },
                new Student
                {
                    Id = 11,
                    DateOfBirth = new System.DateTime(2011, 2, 2),
                    Grade = 10,
                    MathScore = 10,
                    PhysicScore = 10,
                    ChemicalScore = 10
                },
                new Student
                {
                    Id = 3,
                    DateOfBirth = new System.DateTime(2011, 3, 2),
                    Grade = 10,
                    MathScore = 9.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {
                    Id = 4,
                    DateOfBirth = new System.DateTime(2011, 4, 2),
                    Grade = 10,
                    MathScore = 9.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                }
            };

            var result = Program.MonthHaveBestAVGScore(mockData);

            result.Should().Be(2);
        }

        [Fact]
        public void MonthHasBestAVGScore_ShouldReturnCorrectData2()
        {
            var mockData = new List<Student>
            {
                new Student
                {
                    Id = 5,
                    DateOfBirth = new System.DateTime(2011, 1, 2),
                    Grade = 10,
                    MathScore = 9.23,
                    PhysicScore = 9.55,
                    ChemicalScore = 9.23
                },
                new Student
                {
                    Id = 5,
                    DateOfBirth = new System.DateTime(2011, 2, 2),
                    Grade = 10,
                    MathScore = 9.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {
                    Id = 5,
                    DateOfBirth = new System.DateTime(2011, 3, 2),
                    Grade = 10,
                    MathScore = 10,
                    PhysicScore = 10,
                    ChemicalScore = 10
                },
                new Student
                {
                    Id = 5,
                    DateOfBirth = new System.DateTime(2011, 3, 2),
                    Grade = 10,
                    MathScore = 10,
                    PhysicScore = 10,
                    ChemicalScore = 10
                },
                new Student
                {
                    Id = 4,
                    DateOfBirth = new System.DateTime(2011, 4, 2),
                    Grade = 10,
                    MathScore = 9.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                }
            };

            var result = Program.MonthHaveBestAVGScore(mockData);

            result.Should().Be(3);
        }
    }
}

﻿using AutoFixture;
using CSharpTest;
using FluentAssertions;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Test.Tests
{
    public class StudentBirthInFeb2000Tests : SetUpTest
    {
        [Fact]
        public void StudentBirthInFeb2000_WithData1Student()
        {
            var mockData = new List<Student>
            {
                new Student
                {
                    DateOfBirth = new System.DateTime(2011, 1, 2),
                },
                new Student
                {
                    DateOfBirth = new System.DateTime(2011, 1, 2),
                },
                new Student
                {
                    DateOfBirth = new System.DateTime(2011, 1, 2),
                },
                new Student
                {
                    Id = 1,
                    Name = "C",
                    DateOfBirth = new System.DateTime(2000, 3, 2),
                },
                new Student
                {
                    Id = 2,
                    Name = "B",
                    DateOfBirth = new System.DateTime(2000, 3, 2),
                },
                new Student
                {
                    Id = 3,
                    Name = "A",
                    DateOfBirth = new System.DateTime(2000, 3, 2),
                },
                new Student
                {
                    DateOfBirth = new System.DateTime(2011, 2, 2),
                },
                new Student
                {
                    DateOfBirth = new System.DateTime(2011, 2, 2),
                },
                new Student
                {
                    DateOfBirth = new System.DateTime(2011, 2, 2),
                },
            };

            var expected = new List<Student>
            {
                new Student
                {
                    Id = 3,
                    Name = "A",
                    DateOfBirth = new System.DateTime(2000, 3, 2),
                },
                new Student
                {
                    Id = 2,
                    Name = "B",
                    DateOfBirth = new System.DateTime(2000, 3, 2),
                },
                new Student
                {
                    Id = 1,
                    Name = "C",
                    DateOfBirth = new System.DateTime(2000, 3, 2),
                },
            };

            var result = Program.StudentBirthInFeb2020(mockData);

            result.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void StudentBirthInFeb2000_ShouldReturnEmpty()
        {
            var mocks = _fixture.Build<Student>()
                .With(x => x.DateOfBirth, new System.DateTime(2000, 3, 1))
                .CreateMany(10)
                .ToList();

            var expected = mocks.OrderBy(x => x.Name).ToList();

            var result = Program.StudentBirthInFeb2020(mocks);

            result.Should().BeEquivalentTo(expected);
        }
    }
}

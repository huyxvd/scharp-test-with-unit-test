using AutoFixture;
using CSharpTest;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Test.Tests
{
    public class Top3EveryGradeTests : SetUpTest
    {
        [Fact]
        public void Top3EveryGrade_WithStudent1Data()
        {
            var mockData = new List<Student>
            {
                new Student
                {
                    Id = 5,
                    Grade = 11,
                    MathScore = 9.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {
                    Id = 7,
                    Grade = 11,
                    MathScore = 7.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {
                    Id = 8,
                    Grade = 11,
                    MathScore = 1.23,
                    PhysicScore = 1.23,
                    ChemicalScore = 1.23
                },
                new Student
                {
                    Id = 9,
                    Grade = 12,
                    MathScore = 9.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {

                    Id = 10,
                    Grade = 12,
                    MathScore = 8.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {

                    Id = 4,
                    Grade = 10,
                    MathScore = 1.23,
                    PhysicScore = 1.23,
                    ChemicalScore = 1.23
                },
                new Student
                {
                    Id = 11,
                    Grade = 12,
                    MathScore = 7.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {
                    Id = 12,
                    Grade = 12,
                    MathScore = 1.23,
                    PhysicScore = 1.23,
                    ChemicalScore = 1.23
                },
                new Student
                {
                    Id = 1,
                    Grade = 10,
                    MathScore = 9.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {
                    Id = 2,
                    Grade = 10,
                    MathScore = 8.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {
                    Id = 6,
                    Grade = 11,
                    MathScore = 8.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {
                    Id = 3,
                    Grade = 10,
                    MathScore = 7.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
            };
            var expected = new List<Student>
            {
                new Student
                {
                    Id = 1,
                    Grade = 10,
                    MathScore = 9.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {
                    Id = 2,
                    Grade = 10,
                    MathScore = 8.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {
                    Id = 3,
                    Grade = 10,
                    MathScore = 7.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {

                    Id = 5,
                    Grade = 11,
                    MathScore = 9.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {
                    Id = 6,
                    Grade = 11,
                    MathScore = 8.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {
                    Id = 7,
                    Grade = 11,
                    MathScore = 7.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {
                    Id = 9,
                    Grade = 12,
                    MathScore = 9.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {

                    Id = 10,
                    Grade = 12,
                    MathScore = 8.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {
                    Id = 11,
                    Grade = 12,
                    MathScore = 7.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
            };

            var result = Program.Top3EveryGrade(mockData);


            result.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void Top3EveryGrade_ShouldReturnCorrectData()
        {
            var mockData = new List<Student>
            {
                new Student
                {
                    Id = 1,
                    Grade = 10,
                    MathScore = 9.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {
                    Id = 2,
                    Grade = 10,
                    MathScore = 8.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {
                    Id = 3,
                    Grade = 10,
                    MathScore = 7.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {
                    Id = 12,
                    Grade = 12,
                    MathScore = 1.23,
                    PhysicScore = 1.23,
                    ChemicalScore = 1.23
                },
                new Student
                {
                    Id = 9,
                    Grade = 12,
                    MathScore = 9.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {

                    Id = 10,
                    Grade = 12,
                    MathScore = 8.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                 new Student
                {

                    Id = 5,
                    Grade = 11,
                    MathScore = 9.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {
                    Id = 8,
                    Grade = 11,
                    MathScore = 1.23,
                    PhysicScore = 1.23,
                    ChemicalScore = 1.23
                },
                new Student
                {
                    Id = 6,
                    Grade = 11,
                    MathScore = 8.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {
                    Id = 7,
                    Grade = 11,
                    MathScore = 7.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {
                    Id = 11,
                    Grade = 12,
                    MathScore = 7.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
            };

            var expected = new List<Student>
            {
                new Student
                {
                    Id = 1,
                    Grade = 10,
                    MathScore = 9.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {
                    Id = 2,
                    Grade = 10,
                    MathScore = 8.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {
                    Id = 3,
                    Grade = 10,
                    MathScore = 7.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {

                    Id = 5,
                    Grade = 11,
                    MathScore = 9.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {
                    Id = 6,
                    Grade = 11,
                    MathScore = 8.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {
                    Id = 7,
                    Grade = 11,
                    MathScore = 7.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {
                    Id = 9,
                    Grade = 12,
                    MathScore = 9.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {

                    Id = 10,
                    Grade = 12,
                    MathScore = 8.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
                new Student
                {
                    Id = 11,
                    Grade = 12,
                    MathScore = 7.23,
                    PhysicScore = 8.23,
                    ChemicalScore = 9.23
                },
            };

            var result = Program.Top3EveryGrade(mockData);


            result.Should().BeEquivalentTo(expected);
        }
    }
}

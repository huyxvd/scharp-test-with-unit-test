using AutoFixture;
using CSharpTest;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Test.Tests
{
    public class FemaleBirtOn6AMTests : SetUpTest
    {
        [Fact]
        public void FemaleBirtOn6AM_ShouldReturnEmpty()
        {
            var student = _fixture.Build<Student>()
                .With(x => x.DateOfBirth, new DateTime(2000, 12, 12, 6, 0, 0))
                .With(x => x.Gender, "female")
                .CreateMany(10)
                .ToList();
            var expected = student.OrderBy(x => x.Id).ToList();

            var result = Program.FemaleBirtOn6AM(student);

            result.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void FemaleBirtOn6AM_ReturnCorrectData()
        {
            var student = _fixture.Build<Student>()
                .With(x => x.Gender, "male")
                .With(x => x.DateOfBirth, new DateTime(2000, 12, 12, 1, 0, 0))
                .CreateMany(10)
                .ToList();

            var correctData = new List<Student>
            {
                new Student
                {
                    Id = 3,
                    Gender = "female",
                    DateOfBirth = new DateTime(2000, 1, 1, 6, 0, 0)
                },
                new Student
                {
                    Id = 4,
                    Gender = "female",
                    DateOfBirth = new DateTime(2000, 1, 1, 6, 0, 0)
                },
                new Student
                {
                    Id = 1,
                    Gender = "female",
                    DateOfBirth = new DateTime(2000, 1, 1, 6, 0, 0)
                },
                new Student
                {
                    Id = 2,
                    Gender = "female",
                    DateOfBirth = new DateTime(2000, 1, 1, 6, 0, 0)
                },
            };

            var mockData = student.Concat(correctData).ToList();

            var expeceted = new List<Student>
            {
                new Student
                {
                    Id = 1,
                    Gender = "female",
                    DateOfBirth = new DateTime(2000, 1, 1, 6, 0, 0)
                },
                new Student
                {
                    Id = 2,
                    Gender = "female",
                    DateOfBirth = new DateTime(2000, 1, 1, 6, 0, 0)
                },
                new Student
                {
                    Id = 3,
                    Gender = "female",
                    DateOfBirth = new DateTime(2000, 1, 1, 6, 0, 0)
                },
                new Student
                {
                    Id = 4,
                    Gender = "female",
                    DateOfBirth = new DateTime(2000, 1, 1, 6, 0, 0)
                },
            };


            var result = Program.FemaleBirtOn6AM(mockData);


            result.Should().BeEquivalentTo(expeceted);
        }

    }
}
